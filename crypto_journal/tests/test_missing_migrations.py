from django.core.management import call_command
from django.test import TestCase


class CommonTestCase(TestCase):
    def test_missing_migrations(self):
        """Имеющиеся миграции актуальны для текущих моделей (нет неучтенных
        изменений)
        """
        # makemigrations --check --dry-run --settings=project.settings
        try:
            call_command('makemigrations', check_changes=True, dry_run=True)
        except SystemExit as e:
            if e.code == 1:
                msg = 'migrations missed. Please run makemigrations command.'
                raise SystemExit(msg)
            raise
