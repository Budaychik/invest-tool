from django.contrib import admin

from .models import CryptoCoin
from .models import Transaction, CryptoActive


class CoinAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')


class TransactionAdmin(admin.ModelAdmin):
    list_display = ('id', 'owner', 'date', 'coin', 'amount', 'dollars_cost', 'rubles_cost', 'purchase_price')


class CryptoActiveAdmin(admin.ModelAdmin):
    list_display = (
        'id', 'owner', 'coin', 'total_dollars_cost', 'total_rubles_cost', 'amount_coins', 'average_cost_dollars',
        'average_cost_rubles')


admin.site.register(CryptoCoin, CoinAdmin)
admin.site.register(Transaction, TransactionAdmin)
admin.site.register(CryptoActive, CryptoActiveAdmin)
