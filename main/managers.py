import decimal
from django.contrib.auth.base_user import BaseUserManager
from django.db import models
from django.db.models import Sum


class StoringValues:
    """Класс является неким словарем, который хранит в себе расчеты значений для передачи их в КриптоАктив"""

    def __init__(self, value_total_dollars, value_total_rub, value_total_amount, average_dollar, average_rubles):
        self.total_dollars_cost = value_total_dollars
        self.total_rubles_cost = value_total_rub
        self.amount_coins = value_total_amount
        self.average_cost_dollars = average_dollar
        self.average_cost_rubles = average_rubles

    @staticmethod
    def keys():
        return [
            "total_dollars_cost",
            "total_rubles_cost",
            "amount_coins",
            "average_cost_dollars",
            "average_cost_rubles",
        ]

    def items(self):
        for key in self.keys():
            yield key, getattr(self, key)

    def __getitem__(self, key):
        return getattr(self, key)


class TransactionManager(models.Manager):
    def calculation_active_by_coin(self, coin, owner):
        """Рассчитывает суммарные затраты в долларах, рублях, а также суммарное кол-во монет
         и рассчитывает среднюю стоимость монеты"""
        dictionary = self.filter(coin=coin, owner=owner).aggregate(
            total_dollars_cost=Sum('dollars_cost'),
            total_rub_cost=Sum('rubles_cost'),
            total_amount_coins=Sum('amount'),
        )

        value_total_dollars = dictionary['total_dollars_cost']
        value_total_rub = dictionary['total_rub_cost']
        value_total_amount = dictionary['total_amount_coins']

        average_dollar = value_total_dollars / decimal.Decimal(value_total_amount)
        average_rubles = value_total_rub / decimal.Decimal(value_total_amount)

        storing_values_field = StoringValues(value_total_dollars, value_total_rub, value_total_amount,
                                             average_dollar, average_rubles)
        return storing_values_field


class UserManager(BaseUserManager):
    def create_user(self, email: str, password: str = None, **kwargs) -> models.Model:
        """
        Создает и сохраняет пользователя с указанными данными.
        """

        if not email:
            raise ValueError('Пользователи должны иметь адрес электронной почты')

        user = self.model(
            email=self.normalize_email(email),
            **kwargs
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email: str, password: str = None, **kwargs):
        """
        Создает и сохраняет суперпользователя с указанными данными.
        """
        user = self.create_user(
            email,
            password=password,
            **kwargs
        )
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user
