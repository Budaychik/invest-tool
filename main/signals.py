from django.dispatch import receiver
from django.db.models.signals import post_save, post_delete
from .models import Transaction, CryptoActive


@receiver(post_save, sender=Transaction)
def mechanics_calculation(**kwargs):
    """При добавлении, изменении транзакции создается (если нет) КриптоАктив
     и по транзакциям рассчитываются его поля. """
    instance = kwargs['instance']
    storing_value_fields = Transaction.objects.calculation_active_by_coin(coin=instance.coin, owner=instance.owner)

    actives = CryptoActive.objects.filter(coin=instance.coin, owner=instance.owner)
    if not actives:
        CryptoActive.objects.create(
            **storing_value_fields,
            coin=instance.coin,
            owner=instance.owner
        )
    else:
        active: CryptoActive = actives[0]
        active.update(**storing_value_fields)


@receiver(post_delete, sender=Transaction)
def post_delete_transaction_field_recalculation(**kwargs):
    """При удалении транзакции поля КриптоАктива пересчитываются.
       При удалении всех транзакций удаляется и КриптоАктив"""
    instance = kwargs['instance']

    transactions = Transaction.objects.filter(coin=instance.coin, owner=instance.owner)
    if not transactions:
        CryptoActive.objects.filter(coin=instance.coin, owner=instance.owner).delete()

    else:
        storing_value_fields = Transaction.objects.calculation_active_by_coin(coin=instance.coin, owner=instance.owner)
        CryptoActive.objects.get(coin=instance.coin, owner=instance.owner).update(**storing_value_fields)
