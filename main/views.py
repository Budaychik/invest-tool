from rest_framework import status, permissions, viewsets
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.response import Response
from rest_framework.views import APIView
from .serializers import TransactionSerializer, RegistrationSerializer, LoginSerializer, CryptoActiveSerializer
from .models import Transaction, CryptoActive
from .permissions import IsOwner
from .filter import TransactionFilter
from .tasks import registration_is_successful


class TransactionViewSet(viewsets.ModelViewSet):
    permission_classes = IsOwner,
    serializer_class = TransactionSerializer
    filter_backends = (DjangoFilterBackend,)
    filterset_class = TransactionFilter

    def get_queryset(self):
        transactions = Transaction.objects.filter(owner=self.request.user)
        return transactions


class CryptoActiveViewSet(viewsets.ModelViewSet):
    permission_classes = IsOwner,
    serializer_class = CryptoActiveSerializer

    def get_queryset(self):
        active = CryptoActive.objects.filter(owner=self.request.user)
        return active


class RegistrationAPIView(APIView):
    permission_classes = permissions.AllowAny,
    serializer_class = RegistrationSerializer

    def post(self, request):
        user = request.data.get('user', {})
        serializer = self.serializer_class(data=user)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        registration_is_successful.delay(request.user.id)
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class LoginAPIView(APIView):
    permission_classes = permissions.AllowAny,
    serializer_class = LoginSerializer

    def post(self, request):
        user = request.data.get('user', {})
        serializer = self.serializer_class(data=user)
        serializer.is_valid(raise_exception=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
