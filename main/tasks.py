from celery.utils.log import get_logger
from django.core.mail import send_mail
from celery_app import app
from .models import User
from crypto_journal import settings


@app.task()
def registration_is_successful(user_id):
    """Задача для отправки уведомления по электронной почте при успешном создании аккаунта. """
    user = User.objects.get(id=user_id)
    subject = 'Успешная регистрация.'
    message = """Уважаемый {},
        Вы успешно зарегистрировались на Invest-Tool.com!
        Вход в личный кабинет: http://localhost:8000/api/v1/user/login/""".format(user.username)
    to_email = user.email

    mail_sent = send_mail(
        subject=subject,
        message=message,
        from_email=settings.EMAIL_HOST_USER,
        recipient_list=[to_email]
    )

    return mail_sent


@app.task()
def interval_task():
    logger = get_logger(__name__)
    logger.debug("clearing ...")
