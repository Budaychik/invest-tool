from django.urls import path, include
from main import views
from rest_framework.routers import DefaultRouter
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView, TokenVerifyView

router = DefaultRouter()
router.register(r'transactions', views.TransactionViewSet, basename='transactions')
router.register(r'active', views.CryptoActiveViewSet, basename='active')

urlpatterns = [
    path('', include(router.urls)),
    path('user/', views.RegistrationAPIView.as_view(), name='registration'),
    path('user/login/', views.LoginAPIView.as_view(), name='login'),
    path('token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('token/verify/', TokenVerifyView.as_view(), name='token_verify'),
]
