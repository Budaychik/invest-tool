from django_filters import rest_framework as filters
from .models import Transaction


class CharFilterInFilter(filters.BaseInFilter, filters.CharFilter):
    pass


class TransactionFilter(filters.FilterSet):
    coin = CharFilterInFilter(field_name='coin__name', lookup_expr='in')
    amount = filters.RangeFilter()

    class Meta:
        model = Transaction
        fields = ['coin', 'amount']
