from django.test import TestCase
from freezegun import freeze_time

from main.models import Transaction, CryptoCoin
from django.utils import timezone


class ModelTransactionTestCase(TestCase):
    def setUp(self):
        self.coin = CryptoCoin.objects.create(name='KKK')

    @freeze_time(timezone.now())
    def test_auto_setting_date_if_field_empty(self):
        transaction = Transaction.objects.create(
            rubles_cost=1,
            purchase_price=1,
            amount=1,
            dollars_cost=1,
            coin=self.coin
        )

        self.assertEqual(timezone.now(), transaction.date)
