from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase, APIClient
from main.models import User, Transaction, CryptoCoin, CryptoActive
from rest_framework_simplejwt.tokens import RefreshToken
from main.tasks import registration_is_successful
from django.core import mail


class AccountTests(APITestCase):
    def test_registration_and_login_account(self):
        """Тест на возможность регистрировать аккаунт и входить в него"""
        url_registration = reverse('registration')
        data_registration = {"user": {"username": "user10", "email": "user10@user.user", "password": "qweasdzxc"}}
        response_registration = self.client.post(url_registration, data_registration, format='json')
        self.assertEqual(response_registration.status_code, status.HTTP_201_CREATED)

        url_login = reverse('login')
        data_login = {"user": {"email": "user10@user.user", "password": "qweasdzxc"}}
        response_login = self.client.post(url_login, data_login, format='json')
        self.assertEqual(response_login.status_code, status.HTTP_200_OK)


class TransactionsTests(APITestCase):
    def setUp(self) -> None:
        user = User.objects.create_user(email='admin1@admin.com', password='adminadmin')
        user.save()

        coin = CryptoCoin.objects.get(name='BTC')

        self.transaction = Transaction.objects.create(
            rubles_cost=1,
            purchase_price=1,
            amount=1,
            dollars_cost=1,
            coin=coin,
            owner=user
        )

        self.invalid_data = {
            "purchase_price": 2200.00,
            "amount": 12.00000
        }

        self.valid_data = {
            "coin": coin.id,
            "owner": user.id,
            "dollars_cost": 2300.00,
            "rubles_cost": 14000.00,
            "purchase_price": 2200.00,
            "amount": 12.00000
        }

        self.user_token = RefreshToken.for_user(user)
        self.client = APIClient()

    def test_get_fail_transactions(self):
        """Проверка на авторизованность пользователя для получения списка транзакций (не авторизованный пользователь)"""
        response = self.client.get(reverse('transactions-list'), format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_get_transactions(self):
        """Проверка на авторизованность пользователя для получения списка транзакций (авторизованный пользователь)"""
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.user_token.access_token}')
        response = self.client.get(reverse('transactions-list'), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_invalid_transaction(self):
        """Проверка на возможность создавать транзакции и валидность данных (невалидные данные)"""
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.user_token.access_token}')
        response = self.client.post(reverse('transactions-list'), self.invalid_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_valid_transaction(self):
        """Проверка на возможность создавать транзакции и валидность данных (валидные данные)"""
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.user_token.access_token}')
        response = self.client.post(reverse('transactions-list'), self.valid_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_do_not_exist_transactions_detail(self):
        """Проверка на получение несуществующей транзакции"""
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.user_token.access_token}')
        response = self.client.get(reverse('transactions-detail', kwargs={'pk': 100}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_get_transactions_detail(self):
        """Проверка на получение запрашиваемой транзакции"""
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.user_token.access_token}')
        response = self.client.get(reverse('transactions-detail', kwargs={'pk': self.transaction.id}))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_put_invalid_transactions_detail(self):
        """Проверка на возможность редактировать транзакцию и валидность данных (невалидные данные)"""
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.user_token.access_token}')
        response = self.client.put(reverse('transactions-detail', kwargs={'pk': self.transaction.id}),
                                   self.invalid_data,
                                   format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_put_valid_transactions_detail(self):
        """Проверка на возможность редактировать транзакцию и валидность данных (валидные данные)"""
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.user_token.access_token}')
        response = self.client.put(reverse('transactions-detail', kwargs={'pk': self.transaction.id}), self.valid_data,
                                   format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_delete_transactions_detail(self):
        """Проверка на возможность удалять транзакции"""
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.user_token.access_token}')
        response = self.client.delete(reverse('transactions-detail', kwargs={'pk': self.transaction.id}))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)


class CryptoActiveTests(APITestCase):
    def setUp(self) -> None:
        user = User.objects.create_user(email='admin1@admin.com', password='adminadmin')
        user.save()

        self.user_token = RefreshToken.for_user(user)
        self.client = APIClient()

        coin = CryptoCoin.objects.get(name='BTC')

        self.active = CryptoActive.objects.create(
            total_dollars_cost=1000.56,
            total_rubles_cost=3200.13,
            amount_coins=20.1,
            average_cost_dollars=13131,
            average_cost_rubles=11312,
            coin=coin,
            owner=user
        )
        self.invalid_data = {
            'total_dollars_cost': 1000.56,
            'total_rubles_cost': 3200.13,
        }

        self.valid_data = {
            'total_dollars_cost': 1000.56,
            'total_rubles_cost': 3200.13,
            'amount_coins': 20.1,
            'average_cost_dollars': 13131,
            'average_cost_rubles': 11312,
            'coin': coin.id,
            'owner': user.id
        }

    def test_get_fail_active(self):
        """Проверка на авторизованность пользователя для получения списка активов (не авторизованный пользователь)"""
        response = self.client.get(reverse('active-list'), format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_get_active(self):
        """Проверка на авторизованность пользователя для получения списка активов (авторизованный пользователь)"""
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.user_token.access_token}')
        response = self.client.get(reverse('active-list'), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_active_detail(self):
        """Проверка на получение запрашиваемого актива"""
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.user_token.access_token}')
        response = self.client.get(reverse('active-detail', kwargs={'pk': self.active.id}))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_put_invalid_active_detail(self):
        """Проверка на возможность редактировать актив и валидность данных (невалидные данные)"""
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.user_token.access_token}')
        response = self.client.put(reverse('active-detail', kwargs={'pk': self.active.id}),
                                   self.invalid_data,
                                   format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_put_valid_active_detail(self):
        """Проверка на возможность редактировать актив и валидность данных (валидные данные)"""
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.user_token.access_token}')
        response = self.client.put(reverse('active-detail', kwargs={'pk': self.active.id}), self.valid_data,
                                   format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_delete_active_detail(self):
        """Проверка на возможность удалять актив"""
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.user_token.access_token}')
        response = self.client.delete(reverse('active-detail', kwargs={'pk': self.active.id}))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)


class CeleryTaskTest(APITestCase):
    def test_registration_is_successful(self):
        """Тестирует отправку писем после регистрации пользователя"""
        url_registration = reverse('registration')
        data_registration = {"user": {"username": "celery", "email": "celery@yandex.com", "password": "qweasdzxc"}}
        response_registration = self.client.post(url_registration, data_registration, format='json')
        self.assertEqual(response_registration.status_code, status.HTTP_201_CREATED)

        user = User.objects.get(username='celery')

        result = registration_is_successful.delay(user.id)

        self.assertTrue(result.successful())
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, "Успешная регистрация.")
