from django.core.exceptions import ValidationError
from django.test import TestCase

from main.models import CryptoCoin


class ModelTestCase(TestCase):
    def test_model(self):
        with self.assertRaises(ValidationError):
            CryptoCoin.objects.create(name='lll')
