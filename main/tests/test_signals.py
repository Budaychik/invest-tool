import decimal

from django.test import TestCase
from main.models import Transaction, CryptoCoin, CryptoActive, User


class SignalsTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.coin = CryptoCoin.objects.create(name='KKK')

        cls.user1 = User.objects.create_user(email='admin1@admin.com', password='adminadmin')
        cls.user2 = User.objects.create_user(email='sonjang1@mail.com', username='sonjang', password='bkmz2002')

    def test_cryptoactive_created_when_transaction_added(self):
        """ При создании транзакции должен создаться КА пользователя если его нет,
        а его поля должны рассчитаться """
        checking_active_user1 = CryptoActive.objects.filter(coin=self.coin, owner=self.user1).exists()
        checking_active_user2 = CryptoActive.objects.filter(coin=self.coin, owner=self.user2).exists()
        self.assertFalse(checking_active_user1)
        self.assertFalse(checking_active_user2)

        Transaction.objects.create(
            rubles_cost=27,
            purchase_price=2,
            amount=3,
            dollars_cost=12,
            coin=self.coin,
            owner=self.user1
        )

        Transaction.objects.create(
            rubles_cost=27,
            purchase_price=2,
            amount=10,
            dollars_cost=123,
            coin=self.coin,
            owner=self.user2
        )

        checking_active_user1 = CryptoActive.objects.filter(coin=self.coin, owner=self.user1).exists()
        checking_active_user2 = CryptoActive.objects.filter(coin=self.coin, owner=self.user2).exists()

        self.assertTrue(checking_active_user1)
        self.assertTrue(checking_active_user2)

    def test_when_created_transaction_if_active_exists_fields_recalculated(self):
        """При создании транзакции если КА для данной монеты и пользователя существует
         - его поля должны пересчитываться"""
        Transaction.objects.create(
            rubles_cost=100,
            purchase_price=34,
            amount=14,
            dollars_cost=205,
            coin=self.coin,
            owner=self.user1
        )

        Transaction.objects.create(
            rubles_cost=10,
            purchase_price=10,
            amount=10,
            dollars_cost=10,
            coin=self.coin,
            owner=self.user2
        )

        checking_active_user1 = CryptoActive.objects.filter(coin=self.coin, owner=self.user1).exists()
        checking_active_user2 = CryptoActive.objects.filter(coin=self.coin, owner=self.user2).exists()
        self.assertTrue(checking_active_user1)
        self.assertTrue(checking_active_user2)

        Transaction.objects.create(
            rubles_cost=27,
            purchase_price=2,
            amount=3,
            dollars_cost=12,
            coin=self.coin,
            owner=self.user1
        )

        Transaction.objects.create(
            rubles_cost=10,
            purchase_price=10,
            amount=10,
            dollars_cost=10,
            coin=self.coin,
            owner=self.user2
        )

        obj_active_user1 = CryptoActive.objects.get(coin=self.coin, owner=self.user1)
        self.assertEqual(obj_active_user1.total_dollars_cost, 217)
        self.assertEqual(obj_active_user1.total_rubles_cost, 127)
        self.assertEqual(obj_active_user1.amount_coins, 17)
        self.assertEqual(obj_active_user1.average_cost_dollars, decimal.Decimal("12.765"))
        self.assertEqual(obj_active_user1.average_cost_rubles, decimal.Decimal("7.471"))

        obj_active_user2 = CryptoActive.objects.get(coin=self.coin, owner=self.user2)
        self.assertEqual(obj_active_user2.total_dollars_cost, 20)
        self.assertEqual(obj_active_user2.total_rubles_cost, 20)
        self.assertEqual(obj_active_user2.amount_coins, 20)
        self.assertEqual(obj_active_user2.average_cost_dollars, decimal.Decimal("1"))
        self.assertEqual(obj_active_user2.average_cost_rubles, decimal.Decimal("1"))

    def test_when_editing_transaction_active_recalculated(self):
        """При редактировании транзакции поля КА должны пересчитываться"""
        tnx_user1 = Transaction.objects.create(
            rubles_cost=24,
            purchase_price=2,
            amount=3,
            dollars_cost=12,
            coin=self.coin,
            owner=self.user1
        )

        tnx_user2 = Transaction.objects.create(
            rubles_cost=10,
            purchase_price=10,
            amount=10,
            dollars_cost=10,
            coin=self.coin,
            owner=self.user2
        )

        obj_active_user1 = CryptoActive.objects.get(coin=self.coin, owner=self.user1)
        self.assertEqual(obj_active_user1.total_dollars_cost, 12)
        self.assertEqual(obj_active_user1.total_rubles_cost, 24)
        self.assertEqual(obj_active_user1.amount_coins, 3)
        self.assertEqual(obj_active_user1.average_cost_dollars, 4)
        self.assertEqual(obj_active_user1.average_cost_rubles, 8)

        obj_active_user2 = CryptoActive.objects.get(coin=self.coin, owner=self.user2)
        self.assertEqual(obj_active_user2.total_dollars_cost, 10)
        self.assertEqual(obj_active_user2.total_rubles_cost, 10)
        self.assertEqual(obj_active_user2.amount_coins, 10)
        self.assertEqual(obj_active_user2.average_cost_dollars, 1)
        self.assertEqual(obj_active_user2.average_cost_rubles, 1)

        tnx_user1.amount = 4
        tnx_user1.dollars_cost = 64
        tnx_user1.rubles_cost = 16
        tnx_user1.purchase_price = 12
        tnx_user1.save()

        tnx_user2.amount = 20
        tnx_user2.dollars_cost = 20
        tnx_user2.rubles_cost = 20
        tnx_user2.purchase_price = 20
        tnx_user2.save()

        obj_active_user1 = CryptoActive.objects.get(coin=self.coin, owner=self.user1)
        self.assertEqual(obj_active_user1.total_dollars_cost, 64)
        self.assertEqual(obj_active_user1.total_rubles_cost, 16)
        self.assertEqual(obj_active_user1.amount_coins, 4)
        self.assertEqual(obj_active_user1.average_cost_dollars, 16)
        self.assertEqual(obj_active_user1.average_cost_rubles, 4)

        obj_active_user2 = CryptoActive.objects.get(coin=self.coin, owner=self.user2)
        self.assertEqual(obj_active_user2.total_dollars_cost, 20)
        self.assertEqual(obj_active_user2.total_rubles_cost, 20)
        self.assertEqual(obj_active_user2.amount_coins, 20)
        self.assertEqual(obj_active_user2.average_cost_dollars, 1)
        self.assertEqual(obj_active_user2.average_cost_rubles, 1)

    def test_when_delete_transaction_active_recalculated(self):
        """При удалении транзакции поля КА должны пересчитываться"""
        transaction_one = Transaction.objects.create(
            rubles_cost=24,
            purchase_price=2,
            amount=5,
            dollars_cost=12,
            coin=self.coin,
            owner=self.user1
        )

        transaction_two = Transaction.objects.create(
            rubles_cost=20,
            purchase_price=20,
            amount=20,
            dollars_cost=20,
            coin=self.coin,
            owner=self.user2
        )

        Transaction.objects.create(
            rubles_cost=10,
            purchase_price=10,
            amount=10,
            dollars_cost=10,
            coin=self.coin,
            owner=self.user2
        )

        Transaction.objects.create(
            rubles_cost=66,
            purchase_price=5,
            amount=5,
            dollars_cost=105,
            coin=self.coin,
            owner=self.user1
        )

        obj_active_user1 = CryptoActive.objects.get(coin=self.coin, owner=self.user1)
        self.assertEqual(obj_active_user1.total_dollars_cost, 117)
        self.assertEqual(obj_active_user1.total_rubles_cost, 90)
        self.assertEqual(obj_active_user1.amount_coins, 10)
        self.assertEqual(obj_active_user1.average_cost_dollars, decimal.Decimal("11.7"))
        self.assertEqual(obj_active_user1.average_cost_rubles, decimal.Decimal("9"))

        transaction_one.delete()

        obj_active_user2 = CryptoActive.objects.get(coin=self.coin, owner=self.user2)
        self.assertEqual(obj_active_user2.total_dollars_cost, 30)
        self.assertEqual(obj_active_user2.total_rubles_cost, 30)
        self.assertEqual(obj_active_user2.amount_coins, 30)
        self.assertEqual(obj_active_user2.average_cost_dollars, decimal.Decimal("1"))
        self.assertEqual(obj_active_user2.average_cost_rubles, decimal.Decimal("1"))

        transaction_two.delete()

        obj_active_user1 = CryptoActive.objects.get(coin=self.coin, owner=self.user1)
        self.assertEqual(obj_active_user1.total_dollars_cost, 105)
        self.assertEqual(obj_active_user1.total_rubles_cost, 66)
        self.assertEqual(obj_active_user1.amount_coins, 5)
        self.assertEqual(obj_active_user1.average_cost_dollars, decimal.Decimal("21"))
        self.assertEqual(obj_active_user1.average_cost_rubles, decimal.Decimal("13.2"))

        obj_active_user2 = CryptoActive.objects.get(coin=self.coin, owner=self.user2)
        self.assertEqual(obj_active_user2.total_dollars_cost, 10)
        self.assertEqual(obj_active_user2.total_rubles_cost, 10)
        self.assertEqual(obj_active_user2.amount_coins, 10)
        self.assertEqual(obj_active_user2.average_cost_dollars, decimal.Decimal("1"))
        self.assertEqual(obj_active_user2.average_cost_rubles, decimal.Decimal("1"))

    def test_correctness_fields_calculation(self):
        """Поля должны рассчитываться корректно (включая усреднение)"""
        Transaction.objects.create(
            rubles_cost=1234.56,
            purchase_price=1000.74,
            amount=23.13,
            dollars_cost=2046.24,
            coin=self.coin
        )

        Transaction.objects.create(
            rubles_cost=10546.32,
            purchase_price=3756.43,
            amount=87.2,
            dollars_cost=9089.99,
            coin=self.coin
        )

        obj_active = CryptoActive.objects.get(coin=self.coin)

        self.assertEqual(obj_active.total_dollars_cost, decimal.Decimal("11136.23"))
        self.assertEqual(obj_active.total_rubles_cost, decimal.Decimal("11780.88"))
        self.assertEqual(obj_active.amount_coins, decimal.Decimal("110.33"))
        self.assertEqual(obj_active.average_cost_dollars, decimal.Decimal("100.936"))
        self.assertEqual(obj_active.average_cost_rubles, decimal.Decimal("106.779"))

    def test_when_deleting_all_transactions_must_delete_cryptoactive(self):
        """При удалении всех транзакций должен удаляться и КА"""
        transaction_one = Transaction.objects.create(
            rubles_cost=1000.56,
            dollars_cost=3200.13,
            purchase_price=2600.23,
            amount=20.1,
            coin=self.coin
        )

        transaction_two = Transaction.objects.create(
            rubles_cost=21000.56,
            dollars_cost=6704.13,
            purchase_price=7696.23,
            amount=32.7,
            coin=self.coin
        )
        existence_active = CryptoActive.objects.filter(coin=self.coin).exists()
        self.assertTrue(existence_active)

        transaction_one.delete()
        transaction_two.delete()

        checking_transactions = Transaction.objects.filter(coin=self.coin).exists()
        self.assertFalse(checking_transactions)

        checking_active = CryptoActive.objects.filter(coin=self.coin).exists()
        self.assertFalse(checking_active)
