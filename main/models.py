from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.core.exceptions import ValidationError
from django.db import models
from django.utils import timezone
from main.managers import TransactionManager, UserManager


class User(AbstractBaseUser, PermissionsMixin):
    username = models.CharField(verbose_name='Логин', max_length=24, unique=True)
    email = models.EmailField(verbose_name='Электронная почта', max_length=255, unique=True)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    def __str__(self):
        return self.username


def validate_a_register(name):
    if name != name.upper():
        raise ValidationError("Пожалуйста, пишите в верхнем регистре")


class CryptoCoin(models.Model):
    name = models.CharField(
        max_length=5,
        unique=True,
        validators=[validate_a_register]
    )

    def __str__(self):
        return self.name

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        self.full_clean()
        super().save(force_insert, force_update, using, update_fields)

    class Meta:
        verbose_name = 'монету'
        verbose_name_plural = 'монеты'


class Transaction(models.Model):
    date = models.DateTimeField('Дата', blank=True)
    coin = models.ForeignKey(CryptoCoin, on_delete=models.CASCADE)
    owner = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='Владелец', null=True, blank=True)
    dollars_cost = models.DecimalField('Затраты в долларах', max_digits=7, decimal_places=2)
    rubles_cost = models.DecimalField('Затраты в рублях', max_digits=9, decimal_places=2)
    purchase_price = models.DecimalField('Стоимость покупки в долларах', max_digits=6, decimal_places=2)
    amount = models.DecimalField('Количество', max_digits=1000, decimal_places=5)
    objects = TransactionManager()

    def __str__(self):
        return str(self.coin)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if not self.date:
            self.date = timezone.now()
        super().save(force_insert, force_update, using, update_fields)

    class Meta:
        ordering = ('-date',)
        verbose_name = 'транзакцию'
        verbose_name_plural = 'транзакции'


class CryptoActive(models.Model):
    coin = models.ForeignKey(CryptoCoin, on_delete=models.CASCADE)
    owner = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='Владелец', null=True)
    total_dollars_cost = models.DecimalField('Общая стоимость затрат в долларах', max_digits=12, decimal_places=2)
    total_rubles_cost = models.DecimalField('Общая стоимость затрат в рублях', max_digits=15, decimal_places=2)
    amount_coins = models.DecimalField('Количество монет', max_digits=1000, decimal_places=5)
    average_cost_dollars = models.DecimalField(
        'Средняя стоимость покупки актива в долларах',
        max_digits=12,
        decimal_places=3
    )
    average_cost_rubles = models.DecimalField(
        'Средняя стоимость покупки актива в рублях',
        max_digits=12,
        decimal_places=3
    )

    def __str__(self):
        return str(self.coin)

    def update(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)
        self.save()

    class Meta:
        verbose_name = 'актив'
        verbose_name_plural = 'активы'
        unique_together = ['coin', 'owner']
