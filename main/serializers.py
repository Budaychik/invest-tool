from django.contrib.auth import authenticate
from rest_framework import serializers
from .models import Transaction, CryptoCoin, User, CryptoActive


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['username']


class CoinSerializer(serializers.ModelSerializer):
    class Meta:
        model = CryptoCoin
        fields = ['name']


class TransactionSerializer(serializers.ModelSerializer):
    coin = serializers.PrimaryKeyRelatedField(queryset=CryptoCoin.objects.all())
    owner = UserSerializer(read_only=True)

    def create(self, validated_data):
        return Transaction.objects.create(owner=self.context['view'].request.user, **validated_data)

    class Meta:
        model = Transaction
        fields = "__all__"


class CryptoActiveSerializer(serializers.ModelSerializer):
    coin = CoinSerializer(read_only=True)
    owner = UserSerializer(read_only=True)

    class Meta:
        model = CryptoActive
        fields = "__all__"


class RegistrationSerializer(serializers.ModelSerializer):
    """Сериализация регистрации нового пользователя"""

    password = serializers.CharField(
        max_length=64,
        min_length=8,
        write_only=True
    )

    class Meta:
        model = User
        fields = ['email', 'username', 'password']

    def create(self, validated_data):
        return User.objects.create_user(**validated_data)


class LoginSerializer(serializers.ModelSerializer):
    email = serializers.CharField(max_length=255)
    username = serializers.CharField(max_length=255, read_only=True)
    password = serializers.CharField(max_length=128, write_only=True)

    def validate(self, attrs):
        email = attrs.get('email', None)
        password = attrs.get('password', None)

        if email is None:
            raise serializers.ValidationError('Пожалуйста, введите электронную почту.')

        if password is None:
            raise serializers.ValidationError('Пожалуйста, введите пароль.')

        user = authenticate(username=email, password=password)

        if user is None:
            raise serializers.ValidationError('Пользователь с этой электронной почтой не существует.')

        if not user.is_active:
            raise serializers.ValidationError('Этот пользователь был деактивирован.')

        return {'email': user.email, 'username': user.username}

    class Meta:
        model = User
        fields = '__all__'
