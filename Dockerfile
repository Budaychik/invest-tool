FROM python:3.10


ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

WORKDIR /usr/src/invest-tool

RUN mkdir /usr/src/invest-tool/static && mkdir /usr/src/invest-tool/media

COPY ./requirements.txt /usr/src/requirements.txt
RUN pip install --upgrade pip
RUN pip install -r /usr/src/requirements.txt

COPY . /usr/src/invest-tool

EXPOSE 8000